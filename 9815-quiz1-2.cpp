//
//  9815-quiz1-2.cpp
//  1124
//
//  Created by Yifan Wu on 11/24/20.
//

#include "9815-quiz1-2.hpp"
#include <iostream>
  
using namespace std;
  
// heap
void heapify(int arr[], int n, int i)
{
    int max = i;
    int l = 2 * i + 1;
    int r = 2 * i + 2;
  
    // left > root
    if (l < n && arr[l] > arr[max])
        max = l;
  
    // right > root
    if (r < n && arr[r] > arr[max])
        max = r;
  
    // largest != root
    if (max != i) {
        swap(arr[i], arr[max]);
  
        // recursive
        heapify(arr, n, max);
    }
}
  
// add an element
// add i in the arr
void add(int arr[], int& n, int i)
{
    n = n + 1;
  
    // add
    arr[n - 1] = i;
    
    heapify(arr, n, n - 1);
}

// remove an element
// remove arr[i] from the arr
void remove(int arr[], int& n, int i)// 0 <= i <= n
{
    // delete the element
    for (int j = i; j < n; ++j)
        arr[j] = arr[j + 1];
    
    n = n - 1;
  
    heapify(arr, n, 0);
}
  
// print
void printArray(int arr[], int n)
{
    for (int i = 0; i < n; ++i)
        cout << arr[i] << " ";
    cout << "\n";
}
  
// main
int main()
{

// array
    int arr[] = {9, 7, 5};
  
    int n = sizeof(arr) / sizeof(arr[0]);
    
// add
    add(arr, n, 8);
    
    printArray(arr, n);
    
// remove
    remove(arr, n, 2);
  
    printArray(arr, n);

// n
    cout << n << endl;
    return 0;
}
